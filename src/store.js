import Vuex from 'vuex'
if (WXEnvironment.platform !== 'Web') {
  Vue.use(Vuex)
}

const store = new Vuex.Store({
  state: {
    isLogged: false,
    title: '',
    description: '',
    taskStatus: '',
    deadline: ''
  },
  mutations: {
    login (state) {
      state.isLogged = true
    },
    setTitle (state, title) {
      state.title = title.title
    },
    setDescription (state, description) {
      state.description = description.description
    },
    setStatus (state, taskStatus) {
      state.taskStatus = taskStatus.taskStatus
    },
    setDeadline (state, deadline) {
      state.deadline = deadline.deadline
    }
  }
})

export default store
